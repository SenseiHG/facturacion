
package facturacion;

import java.util.ArrayList;
import java.util.List;


public class DetalleFactura {
    private int CantidadProducto;
    private Producto producto;
    
    private List<Producto> ListaProductos;
    
    public DetalleFactura(){
        ListaProductos = new ArrayList<>();
    }

    public int getCantidadProducto() {
        return CantidadProducto;
    }

    public void setCantidadProducto(int CantidadProducto) {
        this.CantidadProducto = CantidadProducto;
    }

    public Producto getProducto() {
        return producto;
    }

    public void setProducto(Producto producto) {
        this.producto = producto;
    }
    
    
}
