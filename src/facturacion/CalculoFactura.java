
package facturacion;


public class CalculoFactura {
   
    private double CalcularSubTotal(DetalleFactura detalle){
        return detalle.getCantidadProducto() * detalle.getProducto().getPrecio();
    }
    
    private double CalcularIVA(double Subtotal){
        return Subtotal * 0.12;
    }
    
    public double CalcularTotal(DetalleFactura detalle){
        return CalcularSubTotal(detalle) + CalcularIVA(CalcularSubTotal(detalle));
    }
    
}
