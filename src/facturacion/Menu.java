
package facturacion;

import java.util.Scanner;


public class Menu {
    private int opcion;

    public int getOpcion() {
        return opcion;
    }

    public void setOpcion(int opcion) {
        this.opcion = opcion;
    }
    
    public void MostrarMenuPrincipal(){
        System.out.println("Bienvenido al sistema FACTURA");
        System.out.println("Escriba la opcion");
        System.out.println("1 - Ingresar datos del cliente");
        System.out.println("2 - Ingresar datos de empresa");
        System.out.println("3 - Ingresar datos del producto");
        Scanner s = new Scanner(System.in);
        this.setOpcion(s.nextInt());
        EvaluarOpcion(this.getOpcion());
    }
    private void MenuCliente(){
        System.out.println("Ingresar datos del cliente");
    }
    private void MenuEmpresa(){
        Scanner s = new Scanner(System.in);
        System.out.println("Ingresar datos de la Empresa");
         Empresa Petrovelca = new Empresa();
         System.out.println("Ingrese dirección:");
        Petrovelca.setDireccion(s.nextLine());
         System.out.println("Ingrese el nombre de la empresa:");
        Petrovelca.setNombre(s.nextLine());
        System.out.println("Ingrese el RUC de la empresa:");
        Petrovelca.setRuc(s.nextLine());
        
        this.MostrarMenuPrincipal();
        
    }
    private void MenuProducto(){
        Scanner scanner = new Scanner(System.in);
        Producto producto =new Producto();
        System.out.println("Ingrese el codigo del producto");
        producto.setCodigo(scanner.nextLine());
        System.out.println("Ingrese el nombre del producto");
        producto.setNombre(scanner.nextLine());
        System.out.println("Ingrese el precio del producto");
        producto.setPrecio(scanner.nextDouble());
        
        /*
        DetalleFactura detalle = new DetalleFactura();
        System.out.println("Ingrese la cantidad de productos a facturar");
        detalle.setCantidadProducto(scanner.nextInt());
        detalle.setProducto(producto);
        */
        this.MostrarMenuPrincipal();
    }
    private void EvaluarOpcion(int opcion){
        
        switch(opcion){
            case 1:
                MenuCliente();
                break;
            case 2:
                MenuEmpresa();
                break;
            case 3:
                MenuProducto();
                break;
            default:
                System.exit(0);
        }
    }
}
