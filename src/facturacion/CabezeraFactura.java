
package facturacion;

import java.sql.Time;
import java.util.Date;


public class CabezeraFactura {
    
    private Empresa emprea;
    private Cliente cliente;
    
    private String NumFactura;
    private Date FechaFactura;
    private Time HoraFactura;

    public Empresa getEmprea() {
        return emprea;
    }

    public void setEmprea(Empresa emprea) {
        this.emprea = emprea;
    }

    public Cliente getCliente() {
        return cliente;
    }

    public void setCliente(Cliente cliente) {
        this.cliente = cliente;
    }

    public String getNumFactura() {
        return NumFactura;
    }

    public void setNumFactura(String NumFactura) {
        this.NumFactura = NumFactura;
    }

    public Date getFechaFactura() {
        return FechaFactura;
    }

    public void setFechaFactura(Date FechaFactura) {
        this.FechaFactura = FechaFactura;
    }

    public Time getHoraFactura() {
        return HoraFactura;
    }

    public void setHoraFactura(Time HoraFactura) {
        this.HoraFactura = HoraFactura;
    }
    
    
}
