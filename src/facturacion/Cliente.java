
package facturacion;


public class Cliente {
    private String Nombres;
    private String Apellidos;
    private String Direccion;
    private String CedulaRuc;

    public String getNombres() {
        return Nombres;
    }

    public void setNombres(String Nombres) {
        this.Nombres = Nombres;
    }

    public String getApellidos() {
        return Apellidos;
    }

    public void setApellidos(String Apellidos) {
        this.Apellidos = Apellidos;
    }

    public String getDireccion() {
        return Direccion;
    }

    public void setDireccion(String Direccion) {
        this.Direccion = Direccion;
    }

    public String getCedulaRuc() {
        return CedulaRuc;
    }

    public void setCedulaRuc(String CedulaRuc) {
        this.CedulaRuc = CedulaRuc;
    }
    
    
}
